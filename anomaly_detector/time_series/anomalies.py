from dataclasses import dataclass
from typing import List, Tuple, Callable
from typing import TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from anomaly_detector.time_series.generate import SamplerKwargs

ValueSampler = Callable[[np.ndarray, "SamplerKwargs"], np.ndarray]


@dataclass
class BaseAnomaly:
    num_points_min: 1
    num_points_max: 1
    chance: float = 0.0
    min_occurrence: int = 0

    def gen_ranges(self, total_num_points: int, start_shift: int = 0) -> Tuple[Tuple[int, int], ...]:
        """
        Generates a list of tuples with a start and an end index at which the
        anomalies can be added during dataset generation.
        :param total_num_points: the total number of points the dataset will have.
        :return: generate list of anomaly range tuples.
        """

        ranges = list()

        if self.chance <= 0 and self.min_occurrence <= 0:
            return tuple(ranges)

        num_open_points = max(
            self.min_occurrence * self.num_points_min,
            int(self.chance * total_num_points) + 1)
        while num_open_points > 0:
            if num_open_points < self.num_points_min:
                break

            start_idx = np.random.randint(0, total_num_points) + start_shift
            if self.num_points_min == self.num_points_max:
                num_points = self.num_points_min
            else:
                num_points = np.random.randint(self.num_points_min, self.num_points_max)
            num_open_points -= num_points

            ranges.append((start_idx, start_idx + num_points))

        for i in range(len(ranges)):
            for j in range(len(ranges)):
                if i == j:
                    continue

                # start_i < start_j < end_i
                if ranges[i][0] < ranges[j][0] < ranges[i][1]:
                    ranges[i] = (ranges[i][0], ranges[j][0])

        if ranges[-1][1] > (total_num_points + start_shift):
            ranges[-1] = (ranges[-1][0], total_num_points + start_shift)

        ranges.sort()

        return tuple(ranges)

    def gen_anomaly_points(
            self,
            sample_positions: np.ndarray,
            generator_func: ValueSampler) -> np.ndarray:

        raise NotImplementedError("This is a abstract method!")


@dataclass
class FrequencyChangeAnomaly(BaseAnomaly):
    frequency_mean: float = 0.0
    frequency_std: float = 0.0

    def gen_anomaly_points(
            self,
            sample_positions: np.ndarray,
            generator_func: ValueSampler) -> np.ndarray:

        frequency = np.random.normal(self.frequency_mean, self.frequency_std)
        return generator_func(
            sample_positions,
            frequency=frequency)


@dataclass
class NoiseAnomaly(BaseAnomaly):
    noise_mean: float = 0.0
    noise_std: float = 0.0

    def gen_anomaly_points(
            self,
            sample_positions: np.ndarray,
            generator_func: ValueSampler) -> np.ndarray:

        noise = np.random.normal(self.noise_mean, self.noise_std, size=sample_positions.size)
        return generator_func(sample_positions) + noise


@dataclass
class MeanShiftAnomaly(BaseAnomaly):
    shift_mean: float = 0.0
    shift_std: float = 0.0

    def gen_anomaly_points(
            self,
            sample_positions: np.ndarray,
            generator_func: ValueSampler) -> np.ndarray:

        shift_value = np.random.normal(self.shift_mean, self.shift_std)
        return generator_func(sample_positions) + shift_value


@dataclass
class AmplitudeShiftAnomaly(BaseAnomaly):
    shift_mean: float = 0.0
    shift_std: float = 0.0
    inc_linearly: bool = True

    def gen_anomaly_points(
            self,
            sample_positions: np.ndarray,
            generator_func: ValueSampler) -> np.ndarray:

        shift_value = np.random.normal(self.shift_mean, self.shift_std)

        values = generator_func(sample_positions)

        if shift_value == 0:
            return values

        change = shift_value
        if self.inc_linearly:
            x = sample_positions - sample_positions[0]
            change = shift_value * x + 1

        # randomly select amplitude gets reduced or increased
        if np.random.choice([True, False]):
            return values * change
        return values / change
