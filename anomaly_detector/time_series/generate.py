from enum import Enum, auto
from functools import partial
from typing import Optional, Tuple, Callable, TypedDict, List, Type

import numpy as np
from scipy import signal
from opensimplex import OpenSimplex

from anomaly_detector.time_series.anomalies import BaseAnomaly


class SampleSource(Enum):
    SIN = auto()
    COS = auto()
    SQUARE = auto()
    SAWTOOTH = auto()
    OPEN_SIMPLEX_NOISE = auto()
    SIMPLEX_SIN = auto()
    LINE = auto()


class SamplerKwargs(TypedDict):
    amplitude: Optional[float]
    frequency: Optional[float]
    seed: Optional[int]


class NoiseKwargs(TypedDict):
    noisy: Optional[bool]
    mean: Optional[float]
    std: Optional[float]


Sampler = Callable[[np.ndarray, SamplerKwargs], np.ndarray]
TimeValues = np.ndarray
SampledValues = np.ndarray
IsAnomalyValues = np.ndarray
TimeSeries = Tuple[TimeValues, SampledValues, IsAnomalyValues]


def gen_univariate(
        source: SampleSource,
        num_points: int,
        sequence_size: int,
        interval: float = 1.0,
        start_pos: float = 0.0,
        sampler_kwargs: Optional[SamplerKwargs] = None,
        noise_kwargs: Optional[NoiseKwargs] = None,
        anomalies: Optional[List[BaseAnomaly]] = None) -> TimeSeries:

    if not sampler_kwargs:
        sampler_kwargs = dict()
    if not noise_kwargs:
        noise_kwargs = dict()
    if not anomalies:
        anomalies = list()

    sampler_map = {
        SampleSource.SIN: partial(_wave_sampler, variant="sin"),
        SampleSource.COS: partial(_wave_sampler, variant="cos"),
        SampleSource.SQUARE: partial(_wave_sampler, variant="square"),
        SampleSource.SAWTOOTH: partial(_wave_sampler, variant="sawtooth"),
        SampleSource.OPEN_SIMPLEX_NOISE: _opensimplex_noise_sampler,
        SampleSource.LINE: _line_sampler,
        SampleSource.SIMPLEX_SIN: partial(
            _combination_sampler,
            sample_funcs=[
                partial(_wave_sampler, variant="sin"),
                _opensimplex_noise_sampler])
    }

    return _generate(
        sampler_map[source],
        sampler_kwargs,
        num_points,
        noise_kwargs,
        anomalies,
        sequence_size,
        start_pos,
        interval)


def gen_multivariate():
    raise NotImplementedError()


def _generate(
        sampler: Sampler,
        sampler_kwargs: SamplerKwargs,
        num_points: int,
        noise_kwargs: NoiseKwargs,
        anomalies: List[BaseAnomaly],
        sequence_size: int,
        sample_start_pos: float = 0.0,
        sample_interval: float = 1.0) -> TimeSeries:

    sample_end_pos = num_points * sample_interval + sample_start_pos
    time_vals = np.linspace(sample_start_pos, sample_end_pos, num_points)

    def _sample(x, **kwargs) -> np.ndarray:
        # the {**<var>, **<var2>} is a inline dict merge for python >= 3.5 as
        # usage of | is only possible with python >= 3.9
        y = sampler(x, **{**sampler_kwargs, **kwargs})
        return y

    sampled_vals = _sample(time_vals)
    is_ano = np.full(num_points, False)

    for ano in anomalies:
        ranges = ano.gen_ranges(num_points - 2 * sequence_size, sequence_size)
        for start, end in ranges:
            update_idx = list(range(start, end))
            sampled_vals.put(
                update_idx,
                ano.gen_anomaly_points(
                    time_vals[start:end],
                    _sample))
            is_ano.put(update_idx, np.full(end - start, True))

    if noise_kwargs.get("noisy", False):
        mean = noise_kwargs.get("mean", 0.0)
        std = noise_kwargs.get("std", 0.1)
        sampled_vals += np.random.normal(mean, std, size=num_points)

    return time_vals, sampled_vals, is_ano


def _wave_sampler(
        values: np.ndarray,
        variant: str,
        amplitude: float = 1.0,
        frequency: float = 1.0,
        **kwargs) -> np.ndarray:

    var_map = {
        "sin": np.sin,
        "cos": np.cos,
        "square": signal.square,
        "sawtooth": signal.sawtooth,
    }

    return amplitude * var_map[variant](frequency * values)


def _opensimplex_noise_sampler(
        values: np.ndarray,
        amplitude: float = 1.0,
        frequency: float = 1.0,
        seed: Optional[int] = 0,
        **kwargs) -> np.ndarray:

    values = frequency * values
    noise = OpenSimplex(seed)
    return np.asarray([amplitude * noise.noise2d(0, v) for v in values])


def _combination_sampler(values: np.ndarray, sample_funcs: List[Callable], **kwargs):
    sampled_values = None
    for func in sample_funcs:
        samples = func(values, **kwargs)
        if sampled_values is None:
            sampled_values = samples
        else:
            sampled_values += samples

    return sampled_values


def _line_sampler(
        values: np.ndarray,
        amplitude: float = 1.0,
        frequency: float = 1.0,
        seed: Optional[int] = 0,
        **kwargs) -> np.ndarray:

    return np.full(values.shape, amplitude)
