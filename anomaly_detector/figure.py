import math
from typing import List, Optional

import plotly.graph_objects as go
import numpy as np
from plotly.subplots import make_subplots

from anomaly_detector.detection.utils import group_consecutive_points


def vis_reconstructed(
        values: np.ndarray,
        reconstructed_vals: np.ndarray,
        title: str) -> go.Figure:

    fig = go.Figure()
    fig.update_layout(title=title)

    fig.add_trace(go.Scatter(
        y=values,
        mode="lines+markers",
        name="train data"))

    fig.add_trace(go.Scatter(
        y=reconstructed_vals,
        mode="lines",
        name="reconstructed"))

    return fig


def vis_anomalies(
        values: np.ndarray,
        pred_anos: np.ndarray,
        title: str,
        gt_anos: Optional[np.ndarray] = None,
        rec_values: Optional[np.ndarray] = None,
        group_consecutive_ano_points: bool = True) -> go.Figure:

    fig = go.Figure()
    fig.update_layout(
        title=title,
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=-0.25,
            xanchor="left",
            x=0
        ))

    fig.add_trace(go.Scatter(
        y=values,
        mode="markers",
        name="Input"))

    if rec_values is not None:
        fig.add_trace(go.Scatter(
            y=rec_values,
            mode="lines",
            name="Reconstructed"))

    pred_ano_groups = [pred_anos]
    if group_consecutive_ano_points:
        pred_ano_groups = group_consecutive_points(pred_anos)

    if gt_anos is not None:
        gt_ano_groups = [gt_anos]
        if group_consecutive_ano_points:
            gt_ano_groups = group_consecutive_points(gt_anos)

        for i, ano_indices in enumerate(gt_ano_groups):
            if ano_indices.size == 0:
                continue
            fig.add_trace(go.Scatter(
                x=ano_indices,
                y=values[ano_indices],
                mode="markers",
                name=f"True anomaly ({i})"))

    if pred_ano_groups:
        for i, ano_indices in enumerate(pred_ano_groups):
            if ano_indices.size == 0:
                continue
            fig.add_trace(go.Scatter(
                x=ano_indices,
                y=values[ano_indices],
                mode="markers",
                marker=dict(
                    symbol="circle-open",
                    line=dict(
                        width=2)),
                name=f"Predicted anomaly ({i})"))

    return fig


def grid_vis_anomalies(
        values: List[np.ndarray],
        gt_anos: List[np.ndarray],
        pred_anos: List[np.ndarray],
        cols: int = -1,
        sub_titles: List[str] = None) -> go.Figure:

    legend_groups = set()

    def _scatter_wrapper(**kwargs) -> go.Figure:
        if "name" in kwargs:
            kwargs["showlegend"] = kwargs["name"] not in legend_groups
            kwargs["legendgroup"] = kwargs["name"]

        fig = go.Scatter(**kwargs)

        if "name" in kwargs:
            legend_groups.add(kwargs["name"])

        return fig

    def _plot_points(values, anos, symbol, color, name: str, size: float = 9) -> go.Figure:
        return _scatter_wrapper(
            x=anos,
            y=values[anos],
            name=name,
            mode="markers",
            marker_symbol=symbol,
            marker_line_color=color,
            marker_color=color,
            marker_line_width=3,
            marker_opacity=1.0,
            marker_size=size)

    if not sub_titles:
        sub_titles = [""] * len(values)

    assert len(values) == len(gt_anos) and len(values) == len(sub_titles) and len(values) == len(pred_anos)

    if cols <= 0:
        cols = len(values)

    rows = math.ceil(len(values) / cols)

    fig = make_subplots(rows=rows, cols=cols)

    for curr_row in range(rows):
        for curr_col in range(cols):
            idx = curr_row * cols + curr_col

            if idx >= len(values):
                break

            curr_value = values[idx]
            curr_gt_anos = gt_anos[idx]
            curr_pred_anos = pred_anos[idx]

            fig.add_trace(
                _scatter_wrapper(
                    y=curr_value,
                    mode="markers",
                    name="values",
                    legendgroup="value",
                    marker_color="darkgray",
                    marker_size=5
                ),
                row=curr_row + 1,
                col=curr_col + 1)

            if curr_gt_anos.size:
                fig.add_trace(
                    _plot_points(
                        values=curr_value,
                        anos=curr_gt_anos,
                        symbol="circle",
                        color="royalblue",
                        name="true anomalies",
                        size=5),
                    row=curr_row + 1,
                    col=curr_col + 1)
            if curr_pred_anos.size:
                fig.add_trace(
                    _plot_points(
                        values=curr_value,
                        anos=curr_pred_anos,
                        symbol="circle-open",
                        color="maroon",
                        name="predicted anomalies"),
                    row=curr_row + 1,
                    col=curr_col + 1)

    fig.update_layout(height=250 * len(values), width=800, title_text="Detected anomalies")
    fig.update_layout(
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.02,
            xanchor="right",
            x=1))

    return fig
