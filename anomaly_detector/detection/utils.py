from typing import Tuple, Union, List

import numpy as np


def standardize_train_data(
        data: np.ndarray) \
        -> Tuple[np.ndarray, Union[np.ndarray, float], Union[np.ndarray, float]]:

    mean = np.mean(data)
    std = np.std(data)
    return standardize_data(mean, std, data), mean, std


def standardize_data(mean: float, std: float, data: np.ndarray) -> np.ndarray:
    return (data - mean) / std


def destandardize_data(mean: float, std: float, data: np.ndarray) -> np.ndarray:
    return data * std + mean


def create_sequences(values: np.ndarray, time_steps: int) -> np.ndarray:
    output = []
    for i in range(len(values) - time_steps):
        output.append(values[i : (i + time_steps)])
    return np.stack(output)


def unwind_sequences(sequences: np.ndarray) -> np.ndarray:
    # this only works based on the premise that the sequence contains real
    # duplications and therefore the first seq_len elements can be combined
    # together with every 256th element at and after pos (seq_len * 2 - 1)
    flat = sequences.flatten()
    seq_len = sequences.shape[1]
    first_part = flat[:seq_len]
    second_part = flat[seq_len * 2 - 1::seq_len]
    return np.hstack((first_part, second_part))


def calc_mean_avg_error(x: np.ndarray, y: np.ndarray) -> np.ndarray:
    return np.mean(np.abs(y - x), axis=1)


def calc_mean_squared_error(x: np.ndarray, y: np.ndarray) -> np.ndarray:
    return np.mean(np.square(y - x), axis=1)


def group_consecutive_points(data, stepsize = 1) -> List[np.ndarray]:
    return np.split(
        data,
        np.where(np.diff(data) != stepsize)[0] + 1)
