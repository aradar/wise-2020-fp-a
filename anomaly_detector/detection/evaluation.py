from typing import Dict

import numpy as np


EPSILON = 1e-8
ROUND_DIGITS = 6


class ConfusionMatrix:
    def __init__(self, y_true: np.ndarray, y_pred: np.ndarray):
        self._y_true = y_true
        self._y_pred = y_pred
        self._p = int(y_true.sum())
        self._n = int((~y_true).sum())
        self._tp = int((y_true & y_pred).sum())
        self._tn = int((~y_true & ~y_pred).sum())
        self._fp = int((~y_true & y_pred).sum())
        self._fn = int((y_true & ~y_pred).sum())

    @property
    def tp(self) -> float:
        return self._tp

    @property
    def tn(self) -> float:
        return self._tn

    @property
    def fp(self) -> float:
        return self._fp

    @property
    def fn(self) -> float:
        return self._fn

    @property
    def recall(self) -> float:
        return round(self._tp / (self._p + EPSILON), ROUND_DIGITS)

    @property
    def selectivity(self) -> float:
        return round(self._tn / (self._n + EPSILON), ROUND_DIGITS)

    @property
    def precision(self) -> float:
        return round(self._tp / (self._tp + self._fp + EPSILON), ROUND_DIGITS)

    @property
    def negative_predicted_value(self) -> float:
        return round(self._tn / (self._tn + self._fn + EPSILON), ROUND_DIGITS)

    @property
    def miss_rate(self) -> float:
        return 1 - self.recall

    @property
    def fall_out(self) -> float:
        return 1 - self.selectivity

    @property
    def false_discovery_rate(self) -> float:
        return 1 - self.precision

    @property
    def false_omission_rate(self) -> float:
        return 1 - self.negative_predicted_value

    @property
    def threat_score(self) -> float:
        return round(self._tp / (self._tp + self._fn + self._fp + EPSILON), ROUND_DIGITS)

    @property
    def accuracy(self) -> float:
        return round((self._tp + self._tn) / (self._p + self._n + EPSILON), ROUND_DIGITS)

    @property
    def balanced_accuracy(self) -> float:
        return round((self.precision + self.selectivity) / 2, ROUND_DIGITS)

    @property
    def all_metrics(self) -> Dict[str, float]:
        metrics = [
            "tp", "tn", "fp", "fn",
            "recall", "selectivity", "precision", "negative_predicted_value",
            "miss_rate", "fall_out", "false_discovery_rate", "false_omission_rate",
            "threat_score", "accuracy", "balanced_accuracy"]

        values = dict()
        for m in metrics:
            values[m] = getattr(self, m)

        return values

    def f_beta(self, beta: float = 1) -> float:
        return (self.precision * self.recall) / (beta ** 2 * self.precision + self.recall + EPSILON)
