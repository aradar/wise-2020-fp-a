import math
from typing import Tuple

from tensorflow import keras
from tensorflow.keras import layers


def build_1d_conv_hourglass_ae(
        input_shape: Tuple[int, int, int],
        kernel_size: int = 7,
        initial_filters: int = 16,
        depth: int = 3,
        l2_reg_strength: float = 1e-4,
        dropout_rate: float = 0.0,
        use_batch_norm: bool = True) -> keras.Model:

    if isinstance(kernel_size, float):
        assert kernel_size.is_integer()
        kernel_size = int(kernel_size)
    if isinstance(initial_filters, float):
        assert initial_filters.is_integer()
        initial_filters = int(initial_filters)

    filters_per_lvl = [int(initial_filters / math.pow(2, d)) for d in range(depth)]

    inputs = layers.Input(shape=(input_shape[1], input_shape[2]))

    curr_input = inputs
    for filters in filters_per_lvl:
        curr_input = layers.Conv1D(
            filters=filters,
            kernel_size=kernel_size,
            padding="same",
            strides=2,
            activation="relu",
            kernel_regularizer=keras.regularizers.L2(l2_reg_strength))(curr_input)

        if use_batch_norm:
            curr_input = layers.BatchNormalization()(curr_input)

        if dropout_rate > 0.0:
            curr_input = layers.Dropout(rate=dropout_rate)(curr_input)

    for filters in reversed(filters_per_lvl):
        curr_input = layers.Conv1DTranspose(
            filters=filters,
            kernel_size=kernel_size,
            padding="same",
            strides=2,
            activation="relu",
            kernel_regularizer=keras.regularizers.L2(l2_reg_strength))(curr_input)

        if use_batch_norm:
            curr_input = layers.BatchNormalization()(curr_input)

        if dropout_rate > 0.0:
            curr_input = layers.Dropout(rate=dropout_rate)(curr_input)

    outputs = layers.Conv1DTranspose(filters=1, kernel_size=kernel_size, padding="same")(curr_input)

    return keras.Model(inputs=inputs, outputs=outputs)


def build_lstm_hourglass_ae(
        input_shape: Tuple[int, int, int],
        initial_units: int = 64,
        depth: int = 3,
        l2_reg_strength: float = 0,
        dropout_rate: float = 0.1) -> keras.Model:

    # todo: maybe also support a statefull variant? but this requires a custom train setup/ loop

    if isinstance(initial_units, float):
        assert initial_units.is_integer()
        initial_units = int(initial_units)

    units_per_lvl = [int(initial_units / math.pow(2, d)) for d in range(depth)]

    inputs = layers.Input(shape=(input_shape[1], input_shape[2]))

    curr_input = inputs
    for units in units_per_lvl:
        curr_input = layers.LSTM(
            units=units,
            return_sequences=units != units_per_lvl[-1],
            kernel_regularizer=keras.regularizers.L2(l2_reg_strength),
            recurrent_regularizer=keras.regularizers.L2(l2_reg_strength),  # todo: test if this is useful
            bias_regularizer=keras.regularizers.L2(l2_reg_strength),  # todo: test if this is useful
            )(curr_input)

        if dropout_rate > 0.0:
            curr_input = layers.Dropout(rate=dropout_rate)(curr_input)

    curr_input = layers.RepeatVector(n=input_shape[1])(curr_input)

    for units in reversed(units_per_lvl):
        curr_input = layers.LSTM(
            units=units,
            return_sequences=True,
            kernel_regularizer=keras.regularizers.L2(l2_reg_strength),
            recurrent_regularizer=keras.regularizers.L2(l2_reg_strength),  # todo: test if this is useful
            bias_regularizer=keras.regularizers.L2(l2_reg_strength),  # todo: test if this is useful
            )(curr_input)

        if dropout_rate > 0.0:
            curr_input = layers.Dropout(rate=dropout_rate)(curr_input)

    outputs = layers.TimeDistributed(layers.Dense(units=input_shape[2]))(curr_input)

    return keras.Model(inputs=inputs, outputs=outputs)
