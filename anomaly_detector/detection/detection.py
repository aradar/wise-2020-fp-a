import json
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Tuple, Optional

import numpy as np

from anomaly_detector.detection.evaluation import ConfusionMatrix
from anomaly_detector.detection.model import build_1d_conv_hourglass_ae, build_lstm_hourglass_ae
from anomaly_detector.detection.utils import standardize_data, calc_mean_avg_error, standardize_train_data, \
    create_sequences, unwind_sequences, destandardize_data, calc_mean_squared_error


class AnomalyDetector(ABC):

    def __init__(self, shape: Tuple[int, int, int]):
        self._shape = shape
        self._data_mean = None
        self._data_std = None

    @abstractmethod
    def train(self, train_data: np.ndarray, **kwargs):
        pass

    @abstractmethod
    def detect(self, data: np.ndarray, **kwargs):
        pass

    @abstractmethod
    def save(self, model_dir: Path):
        pass

    @staticmethod
    @abstractmethod
    def load(model_dir: Path) -> "AnomalyDetector":
        pass


class AutoEncoderAD(AnomalyDetector):

    def __init__(
            self,
            num_features: int = 1,
            kernel_size: int = 7,
            depth: int = 3,
            lstm_version: bool = False,
            initial_filter_or_units: int = 16,
            l2_reg_strength: float = 1e-4,
            dropout_rate: float = 0.0,
            sequence_size: int = 256):

        super().__init__((-1, sequence_size, num_features))

        self._init_kwargs = dict(
            num_features=1,
            kernel_size=7,
            depth=3,
            lstm_version=False,
            initial_filter_or_units=16,
            l2_reg_strength=1e-4,
            dropout_rate=0.0,
            sequence_size=256)

        self._threshold = 0.0
        self._data_mean = 0
        self._data_std = 0

        if lstm_version:
            self._model = build_lstm_hourglass_ae(
                input_shape=self._shape,
                initial_units=initial_filter_or_units,
                depth=depth,
                l2_reg_strength=l2_reg_strength,
                dropout_rate=dropout_rate)
        else:
            self._model = build_1d_conv_hourglass_ae(
                input_shape=self._shape,
                kernel_size=kernel_size,
                l2_reg_strength=l2_reg_strength,
                initial_filters=initial_filter_or_units,
                depth=depth)

    def train(
            self,
            train_data: np.ndarray,
            val_data: Optional[np.ndarray] = None,
            loss: str = "mse",
            optimizer="adam",
            batch_size: int = 128,
            epochs: int = 50,
            validation_split: float = 0.1,
            shuffle: bool = True,
            verbose: int = 1):

        train_data, self._data_mean, self._data_std = standardize_train_data(
            train_data)

        if val_data is not None:
            val_data = standardize_data(self._data_mean, self._data_std, val_data)
            val_data = (val_data, val_data)

        train_seqs = create_sequences(train_data, self.sequence_size)
        x = np.expand_dims(train_seqs, axis=-1)  # adds feature dim --> todo: needs to change for multivar

        # todo: maybe a euclidean distance loss?
        self._model.compile(loss=loss, optimizer=optimizer)
        self._model.summary()

        return self._model.fit(
            x,
            x,
            validation_data=val_data,
            batch_size=batch_size,
            epochs=epochs,
            validation_split=validation_split,
            shuffle=shuffle,
            verbose=verbose)  # todo: shuffling timeseries data could making the network understand them harder?

    def find_threshold(
            self,
            train_data: np.ndarray,
            method: str = "max"):

        if not method or method.lower() not in ["max", "three_sigma_rule"]:
            raise ValueError("Argument method must be a string with a value of max or three_sigma_rule!")

        train_data, _, _ = standardize_train_data(train_data)
        train_seqs = create_sequences(train_data, self.sequence_size)
        x = np.expand_dims(train_seqs, axis=-1)  # adds feature dim --> todo: needs to change for multivar

        predicted = self._model.predict(x)
        #error = calc_mean_squared_error(x, predicted).squeeze()
        error = calc_mean_avg_error(x, predicted).squeeze()

        if method == "max":
            self._threshold = np.max(error)
        else:
            # three sigma rule
            self._threshold = np.mean(error) + 3 * np.std(error)

    def find_optimal_threshold(
            self,
            train_data: np.ndarray,
            data: np.ndarray,
            gt_is_ano: np.ndarray,
            recall_importance: float = 2.0,
            num_sample_positions: int = 10):

        train_data, _, _ = standardize_train_data(train_data)
        train_seqs = create_sequences(train_data, self.sequence_size)
        train_x = np.expand_dims(train_seqs, axis=-1)  # adds feature dim --> todo: needs to change for multivar

        train_predicted = self._model.predict(train_x)
        #train_error = calc_mean_squared_error(train_x, train_predicted).squeeze()
        train_error = calc_mean_avg_error(train_x, train_predicted).squeeze()

        lower_limit = np.mean(train_error)
        upper_limit = np.mean(train_error) + 3 * np.std(train_error)

        data, _, _ = standardize_train_data(data)
        seqs = create_sequences(data, self.sequence_size)
        x = np.expand_dims(seqs, axis=-1)  # adds feature dim --> todo: needs to change for multivar

        predicted = self._model.predict(x)
        #error = calc_mean_squared_error(x, predicted).squeeze()
        error = calc_mean_avg_error(x, predicted).squeeze()

        sample_positions = np.arange(
            lower_limit,
            upper_limit,
            (upper_limit - lower_limit) / num_sample_positions)

        scores = list()
        for threshold in sample_positions:
            indices, _ = self._detect(len(data), error, threshold)
            pred_is_ano = np.full(gt_is_ano.shape, False, bool)
            if np.any(indices):
                pred_is_ano[indices] = True

            cm = ConfusionMatrix(gt_is_ano, pred_is_ano)
            scores.append((threshold, cm.f_beta(recall_importance, beta=2)))

        scores = sorted(scores, key=lambda t: t[1])

        print(scores)

        self._threshold = scores[-1][0]

    def reconstruct(
            self,
            data: np.ndarray,
            return_sequences: bool = False,
            return_data: bool = False,
            denormalize: bool = True):

        data = standardize_data(self._data_mean, self._data_std, data)
        pred_seqs = create_sequences(data, self.sequence_size)
        x = np.expand_dims(pred_seqs, axis=-1)

        predicted = self._model.predict(x)

        if denormalize:
            predicted = destandardize_data(self._data_mean, self._data_std, predicted)

        if not return_sequences:
            predicted = unwind_sequences(predicted)

        if return_data:
            return predicted, data, pred_seqs, x

        return predicted

    def _detect(self, data_len: int, error: np.ndarray, threshold: float):
        above_threshold = error > threshold

        # only select points as anomaly if all sequences which contain this
        # value are above the selected threshold
        ano_indices = []
        ano_error_values = []
        for data_idx in range(self.sequence_size - 1, data_len - self.sequence_size + 1):
            if np.all(above_threshold[data_idx - self.sequence_size + 1: data_idx + 1]):
                ano_indices.append(data_idx)
                ano_error_values.append(
                    np.mean(error[data_idx - self.sequence_size + 1: data_idx + 1]))

        return np.asarray(ano_indices), np.asarray(ano_error_values)

    def detect(self, data: np.ndarray, threshold: Optional[float] = None, **kwargs):
        if not threshold:
            threshold = self._threshold

        predicted, data, pred_seqs, x = self.reconstruct(
            data,
            return_sequences=True,
            return_data=True,
            denormalize=False)

        return self._detect(
            len(data),
            #calc_mean_squared_error(x, predicted).squeeze(),
            calc_mean_avg_error(x, predicted).squeeze(),
            threshold)

    def save(self, model_dir: Path):
        config = dict(
            init_kwargs=self._init_kwargs,
            threshold=self.threshold,
            data_mean=self._data_mean,
            data_std=self._data_std)

        model_dir.mkdir(parents=True, exist_ok=True)
        with (model_dir / "anomaly_detector.json").open("w") as f:
            json.dump(config, f, indent=4)
        self._model.save_weights((model_dir / "model.ckpt").as_posix())

    @staticmethod
    def load(model_dir: Path) -> "AutoEncoderAD":
        with (model_dir / "anomaly_detector.json").open("r") as f:
            config = json.load(f)
        ad = AutoEncoderAD(**config["init_kwargs"])
        ad._model.load_weights((model_dir / "model.ckpt").as_posix())
        return ad

    @property
    def threshold(self) -> float:
        return self._threshold

    @property
    def sequence_size(self) -> int:
        return self._shape[1]

    @property
    def num_features_size(self) -> int:
        return self._shape[2]
